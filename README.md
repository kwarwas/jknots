#	JKnots
##	Stosowane technologie:
* Knockout
* TypeScript 
* JavaScript 
* Bootstrap 
* Node.js 
* Grunt.js 
* Bower

## Zadania:
* Knockout + JS: Imię
* Knockout + JS: Imię i nazwisko
* Knockout + TypeScript: Wprowadzenie
* Knockout + TypeScript: Imię i nazwisko
* Knockout + TypeScript: Regulamin
* Knockout + TypeScript: Regulamin i wiek
* Knockout + TypeScript: Reaktor
* Knockout + TypeScript: Pizzeria
* Knockout + TypeScript: Pizzeria + template
* Knockout + TypeScript: Pizzeria + template + filtrowanie
* Knockout + TypeScript: Gravatar

## Treść zadań:
* Napisać aplikację posiadającą pole tekstowe do wpisania imienia. Wyświetlić w tagu div napis „Witaj” + tekst z pola tekstowego. Tag div ma być wyświetlany jeśli pole tekstowe zawiera znaki.
* Napisać aplikację zawierającą dwa pola tekstowe do wpisania: imienia i nazwiska. Wyświetlić w tagu div napis „Witaj” + tekst z pola tekstowego 1 i 2. Tag div ma być wyświetlany, jeśli pole tekstowe 1 lub 2 zawiera znaki.
* Napisać w języku TypeScript i skompilować kod klasy opisującej Pizzerie.
* Używając języka TypeScript rozwiązać problem opisany w zadaniu 2.
* Napisać aplikację posiadająca checkbox oraz przycisk. Przycisk ma być dostępny jeśli użytkownik zaznaczy checkbox.
* Napisać aplikację posiadającą dwie kontrolki checkbox („akceptacja regulaminu” i „mam więcej niż 18 lat”) oraz przycisk. Przycisk ma być dostępny jeśli użytkownik zaznaczy kontrolkę checkbox 1 i 2.
* Napisać aplikację implementującą algorytm wyświetlania stanu rdzenia reaktora jądrowego w elektrowni atomowej wg następujących zasad (w zależności od aktualnej temperatury rdzenia reaktora). Jeśli:
    * t < 100 kolor szary (reaktor wyłączony)
    * t >= 100 i t < 400 kolor zielony (normalna praca reaktora)
    * t >= 400 i t < 700 kolor pomarańczowy (stan alarmowy)
    * t >= 700 kolor czerwony (stan krytyczny)
* Napisać aplikację wyświetlającą ofertę pizzerii. Każda oferta powinna zawierać nazwę pliku, opis oraz zdjęcie oferty.
* Napisać aplikację wyświetlającą ofertę pizzerii. Każda oferta powinna zawierać nazwę pliku, opis oraz zdjęcie oferty. Wyświetlić oferty korzystając z szablonów html.